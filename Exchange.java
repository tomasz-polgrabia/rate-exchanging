import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.lang.Math;
import java.util.Arrays;

/**
 * This solution is a solution of dynamic programming type.
 * It saves calculations of subproblems to efficiently calculate
 * the main problem. It has a disadvantage - memory.
 *
 * Other possible solutions:
 * - evolutonary/genetic algorithms - starting from the best
 * (even not accessible due to coin number limits),
 * - numerical method with linear limits and linear evaluating function
 *   (f.e. Nelder Mead) amount to exchange - (value of coins).
 *   If number of coins not integral there is no possible exchange way.
 */
public class Exchange {

    public static final Integer[] coinTypeValues = new Integer[] {
        1, 2, 5, 10, 20,
        50, 100, 200, 500
    };

    public static final int NR_OF_COIN_TYPES = coinTypeValues.length;

    private static class ExchangeWay {
        // 1, 2, 5, 10, 20
        // 50, 100, 200, 500

        public Long[] coinValues = new Long[NR_OF_COIN_TYPES];
        public Long allCoins() {
            Long coins = 0L;
            for (int i = 0; i < NR_OF_COIN_TYPES; i++) {
                coins += coinValues[i];
            }
            return coins;
        }

    };

    private static void dumpExchange(ExchangeWay exchange) {
        if (exchange == null) {
            System.out.println("[NOT POSSIBLE TO EXCHANGE]");
            return;
        }

        if (exchange.coinValues == null) {
            System.out.println("[BUG] coinValues are null");
            return;
        }

        String[] scoins = new String[NR_OF_COIN_TYPES];
        for (int i = 0; i < NR_OF_COIN_TYPES; i++) {
            Long coins = exchange.coinValues[i];
            scoins[i] = coins.toString();
        }

        System.out.println("Exchange: "
                + String.join(" ", scoins));

    }

    public static void main(String[] args) {
        try {
            Double amount = null;

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Give the amount to exchange: ");
            String samount = br.readLine();
            amount = Double.parseDouble(samount);
            amount = Math.round(amount*100.0)/100.0;
            System.out.println(String.format("Amount to exchange %f", amount));

            Double damount = amount*100.0;
            Integer amountCoins = damount.intValue();

            ExchangeWay[] exchangeTable = new ExchangeWay[amountCoins+1];

            Long[] coinLimits = new Long[NR_OF_COIN_TYPES];
            for (int i = 0; i < NR_OF_COIN_TYPES; i++) {
                System.out.print(
                        String.format("Give the number of %d coin type:",
                            coinTypeValues[i]));

                String sinput = br.readLine();
                coinLimits[i] = Long.parseLong(sinput);
            }

            exchangeTable[0] = new ExchangeWay();
            exchangeTable[0].coinValues = new Long[] {
                0L, 0L, 0L, 0L, 0L,
                    0L, 0L, 0L, 0L
            };

            for (int i = 1; i <= amountCoins; i++) {
                    Long minCoinNr = Long.MAX_VALUE;
                    int  minCoinTypeIdx = -1;
                for (int j = 0; j < NR_OF_COIN_TYPES; j++) {
                    int cidx = i - coinTypeValues[j];
                    if (cidx < 0) {
                        continue;
                    }

                    // check if previous exchange (exchange way of subproblem)
                    // is possible
                    if (exchangeTable[cidx] == null) {
                        continue;
                    }

                    // check if the limit of coins isn't exceeded
                    if (exchangeTable[cidx].coinValues[j] + 1
                            > coinLimits[j]) {
                        continue;
                    }

                    Long coinNr = exchangeTable[cidx].allCoins();
                    if (coinNr < minCoinNr) {
                        minCoinNr = coinNr;
                        minCoinTypeIdx = j;
                    }
                }

                if (minCoinTypeIdx < 0) {
                    // this amount of money cannot be exchange by us
                    continue;
                }

                ExchangeWay prevExchange
                    = exchangeTable[i-coinTypeValues[minCoinTypeIdx]];

                exchangeTable[i] = new ExchangeWay();
                for (int j = 0; j < NR_OF_COIN_TYPES; j++) {
                    exchangeTable[i].coinValues[j] =
                        prevExchange.coinValues[j];
                }
                exchangeTable[i].coinValues[minCoinTypeIdx]++;
            }
            for (int i = 0; i <= amountCoins; i++) {
                System.out.print("" + i + ": ");
                dumpExchange(exchangeTable[i]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
